@extends('layouts.appl')

@section('content')
    {{-- <div class="row">
         <div class="col-md-8 col-md-offset-2">
             <div class="panel panel-default">
                 <div class="panel-heading">{{ ucfirst(config('app.name')) }} Login</div>
                 <div class="panel-body">

                     @if (count($errors) > 0)
                         <div class="alert alert-danger">
                             <strong>Whoops!</strong> There were problems with input:
                             <br><br>
                             <ul>
                                 @foreach ($errors->all() as $error)
                                     <li>{{ $error }}</li>
                                 @endforeach
                             </ul>
                         </div>
                     @endif

                     <form class="form-horizontal"
                           role="form"
                           method="POST"
                           action="{{ url('login') }}">
                         <input type="hidden"
                                name="_token"
                                value="{{ csrf_token() }}">

                         <div class="form-group">
                             <label class="col-md-4 control-label">Email</label>

                             <div class="col-md-6">
                                 <input type="email"
                                        class="form-control"
                                        name="email"
                                        value="{{ old('email') }}">
                             </div>
                         </div>

                         <div class="form-group">
                             <label class="col-md-4 control-label">Password</label>

                             <div class="col-md-6">
                                 <input type="password"
                                        class="form-control"
                                        name="password">
                             </div>
                         </div>

                         <div class="form-group">
                             <div class="col-md-6 col-md-offset-4">
                                 <a href="{{ route('auth.password.reset') }}">Forgot your password?</a>
                             </div>
                         </div>


                         <div class="form-group">
                             <div class="col-md-6 col-md-offset-4">
                                 <label>
                                     <input type="checkbox"
                                            name="remember"> Remember me
                                 </label>
                             </div>
                         </div>

                         <div class="form-group">
                             <div class="col-md-6 col-md-offset-4">
                                 <button type="submit"
                                         class="btn btn-primary"
                                         style="margin-right: 15px;">
                                     Login
                                 </button>
                             </div>
                         </div>
                     </form>
                 </div>
             </div>
         </div>
     </div>--}}
    <section class="login">
        {{--<div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="login-form-wrapper">
                        <h3>Marine Meteorology</h3>
                        <p>Please Enter your Username and Password</p>

                        <div class="form-container">
                            <div class="login-box">
                                <div class="login-logo">
                                    <a href="../../index2.html"><b>Co</b>Ranks</a>
                                </div>
                                <!-- /.login-logo -->
                                <div class="login-box-body">
                                    <p class="login-box-msg">Sign in to start your session</p>

                                    <form method="POST" action="{{ url('login') }}">
                                        {{ csrf_field() }}
                                        <div class="form-group has-feedback">
                                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                                   name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>

                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                                            @endif
                                        </div>
                                        <div class="form-group has-feedback">
                                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                   name="password" placeholder="password" required>

                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                                            @endif
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8">
                                                <div class="checkbox icheck">
                                                    <label>
                                                        <input type="checkbox"> Remember Me
                                                    </label>
                                                </div>
                                            </div>
                                            <!-- /.col -->
                                            <div class="col-xs-4">
                                                <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                                            </div>


                                        </div>

                                    </form>

                                     <div class="social-auth-links text-center">
                                        <p>- OR -</p>
                                        <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i>
                                            Sign in using
                                            Facebook</a>
                                        <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i>
                                            Sign in using
                                            Google+</a>
                                    </div>
                                    <!-- /.social-auth-links -->

                                    <a href="#">I forgot my password</a><br>
                                    <a href="register.html" class="text-center">Register a new membership</a>



                                </div>
                                <!-- /.login-box-body -->
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>--}}


        <router-view></router-view>



    </section>
    {{--  <router-view></router-view>
      <login></login>--}}
@endsection