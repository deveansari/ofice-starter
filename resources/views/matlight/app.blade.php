<!DOCTYPE html>
<html lang="en">

<head>
    @include('matlight.head')

</head>


<body class="hold-transition skin-blue sidebar-mini">
<div id="app">
<div id="wrapper">

{{-- // --}}

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            @if(isset($siteTitle))
                <h3 class="page-title">
                    {{ $siteTitle }}
                </h3>
            @endif

            <div class="row">
                <div class="col-md-12">

                    @if (Session::has('message'))
                        <div class="note note-info">
                            <p>{{ Session::get('message') }}</p>
                        </div>
                    @endif
                    @if ($errors->count() > 0)
                        <div class="note note-danger">
                            <ul class="list-unstyled">
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif


                </div>
            </div>

                @yield('content')



        </section>

    </div>
</div>
</div>

   
{{-- @include('partials.javascripts') --}}

</body>
</html>