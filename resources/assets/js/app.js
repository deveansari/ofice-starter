window.$ = window.jQuery = require('jquery');
window.Laravel = {csrfToken: $('meta[name=csrf-token]').attr("content")};
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
//Vue.component('ExampleComponent', require('./components/ExampleComponent.vue').default);
//let Login = require('./components/Login.vue')
require('./bootstrap');

window.Vue = require('vue');
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

Vue.use(iView, {locale});
locale(lang);

import iView from 'iview';
import {locale, Page} from 'iview';
//Vue.component('DropdownMenu ', DropdownMenu );
import lang from 'iview/dist/locale/en-US';

Vue.use(VueAxios, axios)
import VueAxios from 'vue-axios'


import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
Vue.use(VueMaterial)

import ExampleComponent from "./components/ExampleComponent";
import Login from "./components/Login";
import Register from "./components/Register";
import Drawer from "./components/Drawer";
//Let Login = require('./components/Login.vue')

const routes = [{
    path: '/login',
    name: 'Login',
    component: Login
},
    {
        path: '/Register',
        name: 'Register',
        component: Register
    }
]
const router = new VueRouter({
    mode: 'history',
    routes,
    /* scrollBehavior () {
         return { x: 0, y: 0 }
     }*/

})

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
    el: '#app',
    router,
    components: {
        Login,
        Register,
        Drawer
        //ExampleComponent

    },
    created() {
        console.log('Working Sir!');
    }
});
