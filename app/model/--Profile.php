<?php
namespace App\model;

use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Hash;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\File;
use App\User;

class Profile extends Model implements HasMedia
{
    use Notifiable;
    use HasMediaTrait;
    protected $fillable = ['id', 'user_id', 'prflFname', 'prflLname', 'prflEmail', 'PrflPhone', 'stAddress', 'stAddress2', 'prflState', 'prflCity', 'prflZip', 'timeZone', 'newsSubcrtEmail', 'interests', 'reviewstat_alert', 'revintrct_alert'];

    public function User()
    {
        return $this->belongsTo('App\User');
    }
    public function registerMediaCollections()
    {
        
        $this
        ->addMediaCollection('profileImg')
            
            ->acceptsFile(function (File $file) {
            //return $file->mimeType === 'image/jpeg';
                return in_array($file->mimeType, ['image/jpeg', 'image/png', 'image/bmp', 'image/webp', 'image/jp2']);
            })
            ->singleFile()
            ->registerMediaConversions(function (Media $media) {
                $this->addMediaConversion('prflPic')
                    ->width(170)
                    ->height(170);
            //->sharpen(10);
            });

    }

}
