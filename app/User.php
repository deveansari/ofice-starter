<?php
namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Hash;
//
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\File;

/**
* Class User
*
* @package App
* @property string $name
* @property string $email
* @property string $password
* @property string $remember_token
*/
class User extends Authenticatable implements HasMedia
{
    use Notifiable;
    use HasRoles;
    //
    
    use HasMediaTrait;
    //use Authenticatable
    
    protected $fillable = ['name', 'email', 'password', 'agreed', 'remember_token'];
    
    
    /**
    * Hash password
    * @param $input
    */
    public function setPasswordAttribute($input)
    {
        if ($input)
        $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }
    
    
    public function role()
    {
        return $this->belongsToMany(Role::class, 'role_user');
    }
    
    
    //
    public function registerMediaCollections()
    {

        $this->addMediaCollection('avatar')
            //add options
            ->acceptsFile(function (File $file) {
                //return $file->mimeType === 'image/jpeg';
                return in_array($file->mimeType, ['image/jpeg', 'image/png', 'image/bmp', 'image/webp', 'image/jp2']);
            })
            ->singleFile()
            ->registerMediaConversions(function (Media $media) {
                $this->addMediaConversion('prflPic')
                    ->width(164)
                    ->height(110);

                // $this->addMediaConversion('categoryImageBg')
                //     ->width(900)
                //     ->height(400);
                //->sharpen(10);
            });
    }
    
    
}
